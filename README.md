# Ref Card 03



![GitLab repo-size](https://img.shields.io/badge/dynamic/json?color=informational&label=repo-size&prefix=%20&query=%24.statistics.repository_size&suffix=%20bytes&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Faelelliotbanyard%252Fref-card-03%3Fstatistics%3Dtrue%26private_token%3Dglpat-E58zhQXNKzMaVBQkkmF4)
![GitLab contributors](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=contributors&query=$.length&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Faelelliotbanyard%252Fref-card-03%2Frepository%2Fcontributors%3Fprivate_token%3Dglpat-E58zhQXNKzMaVBQkkmF4)
![GitLab stars](https://img.shields.io/badge/dynamic/json?color=yellow&label=stars&query=$.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Faelelliotbanyard%252Fref-card-03%3Fstatistics%3Dtrue%26private_token%3Dglpat-E58zhQXNKzMaVBQkkmF4)
![GitLab forks](https://img.shields.io/badge/dynamic/json?color=lightgrey&label=forks&query=$.forks_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Faelelliotbanyard%252Fref-card-03%3Fstatistics%3Dtrue%26private_token%3Dglpat-E58zhQXNKzMaVBQkkmF4)



Ref Card 03 is a Spring Boot Application that allows Users to look at jokes.
The Jokes are read from a Database and displayed on the Web Page.

## Prerequisites

Before you begin, ensure you have met the following requirements:
<!--- These are just example requirements. Add, duplicate or remove as required --->
* You have installed version 11 of Java
* You have installed the latest version of Maven
* You have a MariaDB Database running (on your machine or on AWS)

## Installing Ref Card 03

To install Ref Card 03, follow these steps:

```
mvn -f pom.xml clean package
```

## Using Ref Card 03

To use Ref Card 03, follow these steps:


```
java -jar ref-card-03-0.0.1-SNAPSHOT.jar
```

## Using Ref Card 03 Dockerfile

To use the Dockerfile in the Repository you need to have Docker installed and then follow these steps:

```
docker build -t <tag> .
```

```
docker run -d -p <your_port>:8080 -e DB_URL=jdbc:mariadb://<url_to_db> -e DB_USERNAME=<username_for_db> -e DB_PASSWORD=<password_for_db> <tag>
```

## Using Repository with Pipeline

To use this Repositorys CI you have to follow these steps:

1. Create RDS

   1. Press button "Create Database"
   2. Choose MariaDB
   3. Change DB instance identifier and Master username (if you want)
   4. Choose "Auto generate a password"
   5. Make sure to allow Public access under tab Connectivity
   6. Create Database
   7. Make sure to save the Password that is given to you because you can not get it again

2. Setup ECR and push Dockerfile for the first time with tag latest
3. Create Task Definition with Enviroment Variables (DB_URL, DB_USERNAME, DB_PASSWORD)
4. Create Cluster
5. Create Service with Load Balancer
6. Make sure it ALL runs
7. Add Following Enviroment Variables to GitLab CI:
   1. AWS_ACCESS_KEY_ID (found in AWS Details)
   2. AWS_CLUSTER (Name of Cluster you created earlier)
   3. AWS_DEFAULT_REGION (Default region of your AWS)
   4. AWS_SECRET_ACCESS_KEY  (found in AWS Details)
   5. AWS_SERVICE (Name of Service you created earlier)
   6. AWS_SESSION_TOKEN  (found in AWS Details)
   7. CI_AWS_ECR_REGISTRY (url to Registry you created earlier)
   8. CI_AWS_ECR_REPOSITORY_NAME (name of Repository you created earlier)
8. Run GitLab CI

If any of the steps in 2. - 5. are unclear please check the file [Dokumentation_KN_ArchRefCard-01-mit-AWS-Fargate_Banyard_Ael](Dokumentation_KN_ArchRefCard-01-mit-AWS-Fargate_Banyard_Ael.pdf)

## Contributing to Ref Card 03

<!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
To contribute to Ref Card 03, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitLab documentation on [creating a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/).

## Contact

If you want to contact me you can reach me at [ael.banyard@lernende.bbw.ch](mailto:ael.banyard@lernende.bbw.ch).

## License

This project uses the following license: [ GNU AFFERO GENERAL PUBLIC LICENSE ](https://gitlab.com/aelelliotbanyard/ref-card-03/-/blob/main/LICENSE).